/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author pedroaborba
 */
@ManagedBean
@ApplicationScoped
public class RegistroBean extends utfpr.faces.support.PageBean {

    private static final ArrayList<Candidato> candidatosList = new ArrayList();
    
    public RegistroBean() {
    }
    
    public ArrayList<Candidato> getRegistro() {
        return candidatosList;
    }
    
}
